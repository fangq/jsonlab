Commands to build the package, please copy and paste to a terminal

```
VER=2.0
PKG=jsonlab
wget https://github.com/fangq/${PKG}/archive/v${VER}.tar.gz
tar zxvf v${VER}.tar.gz
# remove upstream binary files
cd ${PKG}-${VER}
mkdir inst
mkdir src
rm -rf Contents.m
mv *.m inst
mv ChangeLog.txt NEWS
mv LICENSE_GPLv3.txt COPYING
cd ..

# recreate the orig package
tar zcvf octave-${PKG}_${VER}.orig.tar.gz ${PKG}-${VER}
cd ${PKG}-${VER}

#download the debian packaging files
git clone https://salsa.debian.org/fangq/${PKG}.git debian

#build deb package
debuild -us -uc
```
